package de.sirleon.skull.cmd;

import de.sirleon.skull.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class SkullCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("skull")) {
            if(sender instanceof Player) {
                Player p = (Player) sender;
                if(args.length == 0) {
                    Bukkit.getScheduler().runTaskLaterAsynchronously(Main.getInstance(), new Runnable() {
                        @Override
                        public void run() {
                            ItemStack i = new ItemStack(Material.PLAYER_HEAD);
                            SkullMeta im = (SkullMeta) i.getItemMeta();
                            im.setOwningPlayer(p);
                            i.setItemMeta(im);

                            p.getInventory().addItem(i);

                            p.sendMessage("Dir wurde dein Kopf ins Inventar gegeben");
                        }
                    }, 1);
                } else if (args.length == 1) {
                    String name = args[0];
                    Player target = Bukkit.getPlayer(name);

                    Bukkit.getScheduler().runTaskLaterAsynchronously(Main.getInstance(), new Runnable() {
                        @Override
                        public void run() {
                            ItemStack ip = new ItemStack(Material.PLAYER_HEAD);
                            SkullMeta ipm = (SkullMeta) ip.getItemMeta();
                            ipm.setOwningPlayer(target);
                            ip.setItemMeta(ipm);

                            p.getInventory().addItem(ip);

                            p.sendMessage("Dir wurde der Kopf von " + target.getName() + " ins Inventar gegeben");
                        }
                    }, 1);
                } else {
                    p.sendMessage("/skull");
                    p.sendMessage("/skull <Player>");
                }
            } else {
                sender.sendMessage("Du bist kein Spieler");
            }
        }
        return true;
    }
}
