package de.sirleon.skull;

import de.sirleon.skull.cmd.SkullCommand;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    private static Main instance;

    @Override
    public void onEnable() {

        Bukkit.getConsoleSender().sendMessage("SkullPlugin von SirLeonDE");
        Bukkit.getConsoleSender().sendMessage("Plugin Version: " + this.getDescription().getVersion());

        getCommand("skull").setExecutor(new SkullCommand());

    }

    @Override
    public void onDisable() {

        Bukkit.getConsoleSender().sendMessage("SkullPlugin von SirLeonDE");
        Bukkit.getConsoleSender().sendMessage("Plugin Version: " + this.getDescription().getVersion());

    }

    public static Main getInstance() {
        return instance;
    }
}
